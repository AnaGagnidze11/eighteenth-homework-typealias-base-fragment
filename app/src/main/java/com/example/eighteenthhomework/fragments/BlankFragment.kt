package com.example.eighteenthhomework.fragments


import android.graphics.Color
import com.example.eighteenthhomework.databinding.FragmentBlankBinding
import com.example.eighteenthhomework.viewmodels.TestViewModel

class BlankFragment : BaseFragment<FragmentBlankBinding, TestViewModel>(
    FragmentBlankBinding::inflate,
    TestViewModel::class.java
) {

    private var whichColor = true

    override fun setUpFragment() {
        init()
    }

    private fun init() {
        binding.colorChangerBtn.setOnClickListener {
            if (whichColor) {
                binding.sleepTxt.setTextColor(Color.parseColor("#d173af"))
                whichColor = !whichColor
            } else {
                binding.sleepTxt.setTextColor(Color.parseColor("#FF03DAC5"))
                whichColor = !whichColor
            }

        }
    }
}